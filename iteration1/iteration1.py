# Imports
import cv2
import numpy as np
import matplotlib as plt

# Used for finding the midline of the road 
def interpolate(array1, array2, degree=3, points=100, plot=True):
    minarray1x, maxarray1x = min(array1[:, 0]), max(array1[:, 0])
    newarray1x = np.linspace(minarray1x, maxarray1x, points)
    array1coef = np.polyfit(array1[:, 0], array1[:, 1], degree)
    newarray1y = np.polyval(array1coef, newarray1x)

    minarray2x, maxarray2x = min(array2[:, 0]), max(array2[:, 0])
    newarray2x = np.linspace(minarray2x, maxarray2x, points)
    array2coef = np.polyfit(array2[:, 0], array2[:, 1], degree)
    newarray2y = np.polyval(array2coef, newarray2x)

    midx = [np.mean([newarray1x[i], newarray2x[i]]) for i in range(points)]
    midy = [np.mean([newarray1y[i], newarray2y[i]]) for i in range(points)]

    if plot:
        plt.plot(array1[:, 0], array1[:, 1], c='black')
        plt.plot(array2[:, 0], array2[:, 1], c='black')
        plt.plot(midx, midy, '--', c='black')
        plt.show()

    return np.array([[x, y] for x, y in zip(midx, midy)])

# Captures video
video = cv2.VideoCapture('IMG_2581.mov') 
while True:
    ret, frame = video.read()
    if not ret:
        print("Error receiving frame from video.")
        break
    # Converts video to greyscale
    greyscale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Blurs video
    grey_blurred = cv2.blur(greyscale, (3, 3))

    # Detects edges of video
    lineImageEdges = cv2.Canny(grey_blurred, 10, 550, apertureSize=7)

    # Specifies how the lines are made
    detectedLines = cv2.HoughLinesP(lineImageEdges, 1, np.pi / 180, 100, minLineLength=100, maxLineGap=10)

    # Line detection
    if detectedLines is not None:

        detectedLines = np.uint16(np.around(detectedLines))

        for pt in detectedLines[0, :]:
            a, b, c, d = pt[0], pt[1], pt[2], pt[3]

            cv2.line(grey_blurred, (a, b), (c, d), (0, 255, 0), 2) # drawing line

        # Shows each frame and allows user to cycle through frames by pressing a key
        cv2.imshow("", greyscale)  # will show video
        cv2.waitKey(0)

# Closes window when video ends
video.release()
cv2.destroyAllWindows()
