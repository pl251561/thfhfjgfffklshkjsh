# Imports
import cv2 as cv
import numpy as np

# Captures video
video = cv.VideoCapture('IMG_5962.MOV')
while True:
    ret, frame = video.read()
    if not ret:
        print("Error receiving frame from video.")
        break
    # Converts video to greyscale
    greyscale = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    # Blurs video
    grey_blurred = cv.blur(greyscale, (3, 3))
    # Specifies how the circles are made
    detected_circles = cv.HoughCircles(grey_blurred, cv.HOUGH_GRADIENT, 1, 1000000, param1=50, param2=30,
                                       minRadius=160, maxRadius=210)

    # Creates circles
    if detected_circles is not None:

        # Convert the circle parameters a, b and r to integers
        detected_circles = np.uint16(np.around(detected_circles))

        for pt in detected_circles[0, :]:
            a, b, r = pt[0], pt[1], pt[2]

            # Draw the circumference of the circle
            cv.circle(greyscale, (a, b), r, (0, 255, 0), 2)

            # Indicates the center of the circle created with a dot
            cv.circle(greyscale, (a, b), 1, (0, 0, 255), 3)

        # Shows each frame and allows user to cycle through frames by pressing a key
        cv.imshow("", greyscale)
        cv.waitKey(0)

# Closes window when video ends
video.release()
cv.destroyAllWindows()
