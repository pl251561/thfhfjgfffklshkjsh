# Imports
import cv2
import numpy as np
import matplotlib.pyplot as plt


# interpolate https://stackoverflow.com/questions/49037902/how-to-interpolate-a-line-between-two-other-lines-in-python
# The arguments: array1 and array2 are your 2 input arrays, degree is the degree polynomial you want to fit,
# points is the number of points you want in your midpoint array,
# and plot is a boolean, whether you want to plot it or not.
def interpolate(array1, array2, degree=3, points=100, plot=True):
    minarray1x, maxarray1x = min(array1[:, 0]), max(array1[:, 0])
    newarray1x = np.linspace(minarray1x, maxarray1x, points)
    array1coef = np.polyfit(array1[:, 0], array1[:, 1], degree)
    newarray1y = np.polyval(array1coef, newarray1x)

    minarray2x, maxarray2x = min(array2[:, 0]), max(array2[:, 0])
    newarray2x = np.linspace(minarray2x, maxarray2x, points)
    array2coef = np.polyfit(array2[:, 0], array2[:, 1], degree)
    newarray2y = np.polyval(array2coef, newarray2x)

    midx = [np.mean([newarray1x[i], newarray2x[i]]) for i in range(points)]
    midy = [np.mean([newarray1y[i], newarray2y[i]]) for i in range(points)]

    if plot:
        plt.plot(array1[:, 0], array1[:, 1], c='black')
        plt.plot(array2[:, 0], array2[:, 1], c='black')
        plt.plot(midx, midy, '--', c='black')
        plt.show()

    return np.array([[x, y] for x, y in zip(midx, midy)])


# Reads provided image of lines
lineImage = cv2.imread('Screen Shot 2023-04-03 at 8.08.24 AM.png')

# Converts to grayscale
lineImageGray = cv2.cvtColor(lineImage, cv2.COLOR_BGR2GRAY)

# Detects edges of lines
lineImageEdges = cv2.Canny(lineImageGray, 10, 550, apertureSize=7)

# Detects straight lines
detectedLines = cv2.HoughLinesP(lineImageEdges, 1, np.pi / 180, 100, minLineLength=100, maxLineGap=10)
for line in detectedLines:
    x1, y1, x2, y2 = line[0]
    print(line[0])
    cv2.line(lineImage, (x1, y1), (x2, y2), (255, 0, 0), 2)
    # interpolate(line[0], line[1])

# Displays curved lines
contours, hierarchy = cv2.findContours(lineImageEdges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
cv2.drawContours(lineImage, contours, -1, (255, 0, 0), 3)

# Creates blue line 5 pixels thick
# cv2.line(lineImage,(0,0),(511,511),(255,0,0),5) # coord1, coord2, color, thickness

# Creates image of detected lines and displays it until a key is pressed
cv2.imwrite('LineResult.jpg', lineImage)
cv2.imshow('LineResult.jpg', lineImage)
cv2.waitKey(0)
